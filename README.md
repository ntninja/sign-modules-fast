# Fast post-install kernel module signer

This repository provides pretty fast implementations of a post-installation
kernel module signer using a local CA certificate for user-controlled secure
boot configurations with Desktop Linux.

For a guide on how to set up such a secure boot configuration see the
[Arch Wiki](https://wiki.archlinux.org/title/Unified_Extensible_Firmware_Interface/Secure_Boot#Using_your_own_keys)
– the steps outlined there also work in for other distributions.

The scripts provided here only deal with resigning all kernel modules
(including those compiled on-the-fly by DKMS) on installation of a kernel
update.

The usual advice for doing this involves [invoking the `sign-file` utility](https://www.kernel.org/doc/html/v6.0/admin-guide/module-signing.html)
from the kernel build tools package for each kernel module in turn. This is
not very fast however and that tool does not handle compressed kernel module
files at all. The only way to make it work with those is to decompress each
file, let the utility sign the decompressed file, then (optionally) recompress
it. This is what [the shell script](./sign-modules.sh) in this repository does
– with the extra feature of using [GNU Parallel](https://www.gnu.org/software/parallel/)
to achieve bearable speeds (about 100 modules/second with XZ/LZMA compression
on this system) using multiprocess parallization.

In addition [a Python script](./sign-modules.py) is provided that takes a
different approach:

 1. Kernel modules are decompressed using Python’s standard library format
    support (or either the external [`zstandard`](https://python-zstandard.readthedocs.io/)
    Python package or the `zstd` command-line utility in case of [ZStandard](https://facebook.github.io/zstd/)
    compressed files) and hashed
 2. A `sign-file`-compatible [CMS signature](https://en.wikipedia.org/wiki/Cryptographic_Message_Syntax)
    is generated using the Python [`cryptography`](https://cryptography.io/) and
    [`asn1crypto`](https://github.com/wbond/asn1crypto/) modules – the original
    `sign-file` utility is not used
 3. The signature and mandatory magic string are appended to compressed kernel
    module file without recompressing it entirely – all used compression
    libraries support this mode but none of them appears to expose this facility
    through their command-line interfaces

This, combined with some multi-process parallelization using another one of
Python’s standard library modules, results in speeds of about 260 modules/second
on this system for the same modules and appears to be CPU-bound on the
decompressor side of things. It is unclear whether there would be any further
significant gains by an optimized Rust or C implementation.

## How to use

Ony Debian, copy the [Python](./sign-modules.py) or [Shell](./sign-modules.sh)
as `zz-sign-modules` to the `/etc/kernel/postinst.d/` directory and mark it
as executable. Edit the copied file and adjust the `SIGNING_CRT` and
`SIGNING_KEY` variables towords the start of the file as necessary to point
to the certificate used to sign your kernel image. On our system these reside
in an [fscrypt](https://github.com/google/fscrypt/) protected directory that
is unlocked by the user’s login passphrase, but other configurations are of
course possible.