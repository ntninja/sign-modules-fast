#!/usr/bin/bash
set -eu -o pipefail
shopt -s globstar
shopt -s nullglob

KERNEL_VERSION="$1"
KERNEL_MODDIR="/lib/modules/${KERNEL_VERSION}"
SIGNING_ALG="sha256"
SIGNING_CRT="/etc/secure/efi-keys/system.crt"
SIGNING_KEY="/etc/secure/efi-keys/system.key"
TOOL_SIGN_FILE="${KERNEL_MODDIR}/build/scripts/sign-file"

# Avoid running multiple times
if [ -n "${DEB_MAINT_PARAMS:-}" ]; then
	eval set -- "${DEB_MAINT_PARAMS}"
	if [ -z "$1" ] || [ "$1" != "configure" ]; then
		exit 0
	fi
fi

# Ensure `sign-file` utility has been compiled
#
# This mostly works around an issue with the Liquorix Debian packages which
# ship the `sign-file` utility only in source form.
if ! [[ -x "${TOOL_SIGN_FILE}" ]];
then
	echo "sign-modules: Compiling \`sign-file\` utility on-the-spot (workaround)"
	cc -o "${TOOL_SIGN_FILE}" "${TOOL_SIGN_FILE}.c" -lcrypto
fi

# Perform main operation
#
# Invokes the `sign-file` utility shipped with the kernel headers on each
# kernel module file, decompressing and recompressing them as necessary.
echo "sign-modules: Signing all modules of kernel ${KERNEL_VERSION}"

export SIGNING_ALG SIGNING_KEY SIGNING_CRT TOOL_SIGN_FILE
SIGN_SCRIPT="$(
cat <<'EOF'
	for filepath in "$@";
	do
		case "${filepath}" in
			*.ko)
				"${TOOL_SIGN_FILE}" "${SIGNING_ALG}" "${SIGNING_KEY}" "${SIGNING_CRT}" "${filepath}"
			;;
			*.ko.gz)
				gzip -dfk "${filepath}"
				"${TOOL_SIGN_FILE}" "${SIGNING_ALG}" "${SIGNING_KEY}" "${SIGNING_CRT}" "${filepath%%.gz}"
				gzip -f "${filepath%%.gz}"
			;;
			*.ko.xz)
				xz -dfk "${filepath}"
				"${TOOL_SIGN_FILE}" "${SIGNING_ALG}" "${SIGNING_KEY}" "${SIGNING_CRT}" "${filepath%%.xz}"
				xz -zf -0 "${filepath%%.xz}"
			;;
			*.ko.zst)
				zstd -dfk "${filepath}"
				"${TOOL_SIGN_FILE}" "${SIGNING_ALG}" "${SIGNING_KEY}" "${SIGNING_CRT}" "${filepath%%.zst}"
				zstd -zf --rm "${filepath%%.zst}"
			;;
		esac
	done
	echo -n "."
EOF
)"
parallel -n10 sh -c "${SIGN_SCRIPT}" - -- "${KERNEL_MODDIR}"/**/*.ko{,.gz,.xz,.zst}  # Uses `globstar` and `nullglob` shopts
echo