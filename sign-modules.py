#!/usr/bin/env python3
import concurrent.futures
import gzip
import lzma
import io
import os
import pathlib
import subprocess
import sys
import typing as ty

import asn1crypto.algos
import asn1crypto.cms
import cryptography.x509
import cryptography.hazmat.primitives.asymmetric
import cryptography.hazmat.primitives.hashes
import cryptography.hazmat.primitives.serialization


KERNEL_VERSION = sys.argv[1]
KERNEL_MODDIR = pathlib.Path("/lib/modules") / KERNEL_VERSION
SIGNING_ALG = "sha256"
SIGNING_CRT = "/etc/secure/efi-keys/system.crt"
SIGNING_KEY = "/etc/secure/efi-keys/system.key"
TOOL_SIGN_FILE = KERNEL_MODDIR / "build" / "scripts" / "sign-file"


# Avoid running multiple times
if "configure" not in os.environ.get("DEB_MAINT_PARAMS", "configure").split():
	sys.exit(0)


# Map parameters above to Python :mod:`cryptography` objects
SIGNING_ALG_OBJ = next(
	cls()
	for cls in cryptography.hazmat.primitives.hashes.HashAlgorithm.__subclasses__()
	if cls.name == SIGNING_ALG
)

with open(SIGNING_CRT, "rb") as signing_crt_file:
	SIGNING_CRT_OBJ = cryptography.x509.load_pem_x509_certificate(signing_crt_file.read())

with open(SIGNING_KEY, "rb") as signing_key_file:
	SIGNING_KEY_OBJ = cryptography.hazmat.primitives.serialization.load_pem_private_key(signing_key_file.read(), password=None)


def run(*argv: ty.Union[str, bytes, os.PathLike], **kwargs) -> subprocess.CompletedProcess:
	"""More convenient way to invoke :func:`subprocess.run`"""
	return subprocess.run([
		os.fsencode(arg) if isinstance(arg, os.PathLike) else arg
		for arg in argv
	], check=True, **kwargs)


# Perform main operation
#
# Invokes the ``sign-file`` utility shipped with the kernel headers on each
# kernel module file, decompressing and recompressing them as necessary.

def find_modules(top: pathlib.Path) -> ty.Generator[pathlib.Path, ty.Any, None]:
	"""Scans a directory tree for kernel module files based on their filenames"""
	for dirpath, dirnames, filenames in os.walk(top):
		dirpath = pathlib.Path(dirpath)
		for filename in filenames:
			if filename.endswith(".ko") or filename.endswith(".ko.gz") \
			   or filename.endswith(".ko.xz") or filename.endswith(".ko.zst"):
				yield dirpath / filename

## Uncomment this to hard-code a single kernel module file for testing
#def find_modules(top: pathlib.Path) -> ty.Generator[pathlib.Path, ty.Any, None]:
#	yield top / "updates/dkms/ddcci.ko.xz"


def generate_module_signature(
		modfile: io.RawIOBase,
		crt: cryptography.x509.Certificate,
		key: cryptography.hazmat.primitives.asymmetric.rsa.RSAPrivateKey,
		alg: cryptography.hazmat.primitives.hashes.HashAlgorithm,
) -> bytes:
	"""Generates the signature for a file like `sign-file` / OpenSSL’s CMS
	   support would
	
	This is much faster than said utility for small files since it avoids
	shelling out to for each and every file.
	"""
	# Read and hash existing module contents
	hasher = cryptography.hazmat.primitives.hashes.Hash(alg)
	while len(chunk := modfile.read(io.DEFAULT_BUFFER_SIZE * 100)) > 0:
		hasher.update(chunk)
	digest = hasher.finalize()
	
	# Compute signature value like `sign-file` (OpenSSL CMS defaults) does
	signature = key.sign(
		digest,
		cryptography.hazmat.primitives.asymmetric.padding.PKCS1v15(),
		cryptography.hazmat.primitives.asymmetric.utils.Prehashed(alg),
	)
	
	# Construct CMS ContentInfo envelope for signature like `sign-file` does
	content_info = asn1crypto.cms.ContentInfo()
	content_info["content_type"] = "data"
	
	issuer_and_serial = asn1crypto.cms.IssuerAndSerialNumber()
	issuer_and_serial["issuer"] = asn1crypto.x509.Name.load(crt.issuer.public_bytes())
	issuer_and_serial["serial_number"] = crt.serial_number
	
	digest_algorithm = asn1crypto.algos.DigestAlgorithm()
	digest_algorithm["algorithm"] = asn1crypto.algos.DigestAlgorithmId(alg.name)
	del digest_algorithm["parameters"]
	
	signature_algorithm = asn1crypto.algos.SignedDigestAlgorithm()
	signature_algorithm["algorithm"] = "rsassa_pkcs1v15"  # Hard-coded above
	del signature_algorithm["parameters"]
	
	signer_info = asn1crypto.cms.SignerInfo()
	signer_info["version"] = "v1"
	signer_info["sid"] = issuer_and_serial
	signer_info["digest_algorithm"] = digest_algorithm
	signer_info["signature_algorithm"] = signature_algorithm
	signer_info["signature"] = signature
	
	signed_data = asn1crypto.cms.SignedData()
	signed_data["version"] = "v1"
	signed_data["digest_algorithms"] = [digest_algorithm]
	signed_data["encap_content_info"] = content_info
	signed_data["signer_infos"] = [signer_info]
	
	content_info = asn1crypto.cms.ContentInfo()
	content_info["content_type"] = "signed_data"
	content_info["content"] = signed_data
	
	return content_info.dump()


def sign_module_inner(path: pathlib.Path, opener) -> None:
	# Generate detached signature like the kernel-provided `sign-file` utility
	# would, decompressing the module data on-the-fly
	#
	# This is **much** faster than shelling out to said utility for each of
	# the 6000+ kernel module files shipped with Debian.
	with opener(path, "rb") as modfile:
		signature = generate_module_signature(
			modfile,
			SIGNING_CRT_OBJ,
			SIGNING_KEY_OBJ,
			SIGNING_ALG_OBJ,
		)

	# Append signature to compressed kernel module file without recompressing
	with opener(path, "ab") as modfile:
		modfile.write(signature)
		modfile.write(b"~Module signature appended~\n")

def sign_module(path: pathlib.Path) -> None:
	if path.name.endswith(".ko"):
		sign_module_inner(path, open)
	elif path.name.endswith(".ko.gz"):
		sign_module_inner(path, gzip.open)
	elif path.name.endswith(".ko.xz"):
		sign_module_inner(path, lzma.open)
	elif path.name.endswith(".ko.zst"):
		try:
			import zstandard
		except ModuleNotFoundError:
			# Much slower fallback using the `zstd` utility program involving
			# full recompression of the kernel module file
			decompressed_path = path.parent / path.name.removesuffix(".zstd")
			run("zstd", "-dfk", path)
			sign_module_inner(decompressed_path, open)
			run("zstd", "-zf", "--rm", decompressed_path)
		else:
			sign_module_inner(path, zstandard.open)

print(f"sign-modules: Signing all modules of kernel {KERNEL_VERSION}", end="")
progress_text = ""
with concurrent.futures.ProcessPoolExecutor() as executor:
	module_paths = list(find_modules(KERNEL_MODDIR))
	for count, _ in enumerate(executor.map(sign_module, module_paths, chunksize=100), 1):
		print("\b" * len(progress_text), end="")
		progress_text = f" ({count}/{len(module_paths)})"
		print(progress_text, end="")
print(("\b" * len(progress_text)) + (" " * len(progress_text)))